import sys, os, sqlite3, subprocess, getopt, math, argparse, csv

from os.path import expanduser
import matplotlib.pyplot as plt
import numpy, scipy.optimize, scipy.signal
from pylab import *
from config.Measure_Waveform_Config import measure_waveform_config_from_yaml
""" 
This script automates the characterisation of the directional coupler. 
The script scans the signal generator over different frequencies and VRms values and saves them into a database file
The scope saves the Vrms value for the transmitted and reflected waves.
It stops to allow you to make changes to the setup each time.
Measurements taken are:
Load, open, short, coil
Each component is connected directly to the output port of the directional coupler
"""

####################################################################################################
####################################################################################################

""" Global Variables """

# Useful variables
ON = 1
OFF = 0

SCOPE_TIME_FACTORS = [1, 2, 5, 10, 20, 50, 100, 200, 500]
SCOPE_VOLTAGE_FACTORS = [1, 2, 5, 10, 20, 50, 100, 200, 500]
SCOPE_TIME_BASE_LIST = []
SCOPE_VOLTAGE_LIST = []
FREQ_LIST = []

####################################################################################################
####################################################################################################
#Helper functions

def parser():
    """Perform an arm sweep described by a YAML config file"""
    parser = argparse.ArgumentParser()
    parser.add_argument("config_file", help="YAML config file to read from")
    parser.add_argument(
        "--override", action="store_true", help="Replace the output database if it already exists"
    )

    args = parser.parse_args()
    cfg = measure_waveform_config_from_yaml(args.config_file)

    return [cfg,args.config_file]

#########################################
#########################################

def fit_sin(tt, yy, frequency):
    '''Fit sin to the input time sequence, and return fitting parameters "amp", "omega", "phase", "offset", "freq", "period" and "fitfunc"'''
    tt = numpy.array(tt)
    yy = numpy.array(yy)
    ff = numpy.fft.fftfreq(len(tt), (tt[1]-tt[0]))   # assume uniform spacing
    n = len(ff)
    ff = ff[0:round(n/2):1]
    Fyy = abs(numpy.fft.fft(yy))
    Fyy = Fyy[0:round(n/2):1]
    guess_freq = abs(ff[numpy.argmax(Fyy[1:])+1])   # excluding the zero frequency "peak", which is related to offset
    #guess_freq = frequency
    guess_amp = numpy.std(yy) * 2.**0.5
    guess_offset = numpy.mean(yy)
    guess = numpy.array([guess_amp, 2.*numpy.pi*guess_freq, 1, guess_offset])

    # guess = [guess_amp, (2. * numpy.pi * guess_freq), 0., guess_offset]
    # print(guess)

    def sinfunc(t, A, w, p, c):  return A * numpy.sin(w*t + p) + c

    kwargs = {"maxfev":50000}
    popt, pcov = scipy.optimize.curve_fit(sinfunc, tt, yy, p0=guess,**kwargs)
    # res_leq = scipy.optimize.least_squares(sinfunc, guess, loss="soft_l1",f_scale=0.1,args=(tt,yy))

    A, w, p, c = popt
    f = w/(2.*numpy.pi)

    # testArray = sinfunc(tt,A,w,p,c)
    #
    # if(frequency == 250000):
    #
    #     figure()
    #     plt.plot(tt,yy,tt,testArray)
    #     plt.show()

    fitfunc = lambda t: A * numpy.sin(w*t + p) + c
    return {"amp": abs(A), "omega": w, "phase": p, "offset": c, "freq": f, "period": 1./f, "fitfunc": fitfunc, "maxcov": numpy.max(pcov), "rawres": (guess,popt,pcov)}

def get_num_wavelengths(frequency,time,data,num_lambda=5):
    lambda_calc = (3e8)/frequency
    multi_freq = (3e8)/(5*lambda_calc)
    period = 1 / frequency
    multi_period = 1/multi_freq
    delta_t = time[1]-time[0]
    num_points = round(multi_period / delta_t)

    current_minimum = 1e5
    i = 0
    first_zero_index = 0

    for value in data[0:round(2 * num_points / num_lambda)]:

        if(abs(value) < current_minimum):
            first_zero_index = i
            current_minimum = abs(value)

        i += 1

    #print("Indexes are: {} {}".format(i,i+num_points))

    return {"time":time[first_zero_index:(first_zero_index+num_points)],"data":data[first_zero_index:(first_zero_index+num_points)]}

#########################################
#########################################

def main():

    [cfg,config_file] = parser()

    print(cfg)

    if(cfg.frequency.step == -1):
        num_points = cfg.frequency.num_points
        freqStep = (cfg.frequency.high - cfg.frequency.low) / cfg.frequency.num_points
        FREQ_LIST.append(cfg.frequency.low)
        temp = cfg.frequency.low
    else:
        num_points = round((cfg.frequency.high - cfg.frequency.low) / cfg.frequency.step)
        freqStep = cfg.frequency.step
        FREQ_LIST.append(cfg.frequency.low)
        temp = cfg.frequency.low

    for i in range(1, num_points + 1, 1):
        temp = temp + freqStep
        FREQ_LIST.append(temp)
        print(temp)

    # output file base name
    home = expanduser("~")
    print(home+"/"+cfg.temp_location)

    if not os.path.exists(home + "/" + cfg.temp_location + "/" + cfg.data_folder):
        os.chdir(home + "/" + cfg.temp_location)
        os.mkdir(cfg.data_folder)

    os.chdir(home + "/" + cfg.temp_location + "/" + cfg.data_folder)

    if not os.path.exists(home + "/" + cfg.temp_location + "/" + cfg.data_folder + "/Postprocessing"):
        os.chdir(home + "/" + cfg.temp_location + "/" + cfg.data_folder)
        os.mkdir("Postprocessing")

    vrms_siggen = []
    vrms1 = []
    vrms2 = []
    vrms3 = []
    vrms4 = []
    Vpp1 = []
    Vpp2 = []
    Vpp3 = []
    Vpp4 = []

    # Create an doutput file for the vrms values
    outputFileName = cfg.measurement_name + "_Vrms"

    outputFile = open(outputFileName, 'w')

    #Dynamic header which changes depending upon which channels were active
    channel1_string = "Channel_1 \t " if cfg.channel_1.active else ""
    channel2_string = "Channel_2 \t " if cfg.channel_2.active else ""
    channel3_string = "Channel_3 \t " if cfg.channel_3.active else ""
    channel4_string = "Channel_4 \t " if cfg.channel_4.active else ""
    headerString = "Frequency \t {}{}{}{} \n".format(channel1_string,channel2_string,channel3_string,channel4_string)

    outputFile.write(headerString)

    for frequency in FREQ_LIST:
        #frequency = 100000

        vrms_siggen.append(cfg.siggen.voltage)

        fileName = cfg.measurement_name + "_{}".format(str(int(frequency)))

        input_file = open(fileName,'r')
        inputText = input_file.readlines()[35:]

        print("Read in data from {}".format(fileName))

        reader = csv.reader(inputText)

        time = []

        if (cfg.channel_1.active):
            channel_1 = []
        if(cfg.channel_2.active):
            channel_2 = []
        if (cfg.channel_3.active):
            channel_3 = []
        if (cfg.channel_4.active):
            channel_4 = []

        next(reader)

        for r in reader:
            line = r[0].split("\t")

            time.append(float(line[0]))

            if (cfg.channel_1.active):
                channel_1.append(float(line[1]))
            if (cfg.channel_2.active):
                channel_2.append(float(line[2]))
            if (cfg.channel_3.active):
                channel_3.append(float(line[3]))
            if (cfg.channel_4.active):
                channel_4.append(float(line[4]))


        if (cfg.channel_1.active):
            avg_squared = 0
            sum = 0
            proc_data = get_num_wavelengths(frequency, time, channel_1, 5)
            time1 = proc_data["time"]
            data1 = proc_data["data"]

            # for num in channel_1: sum = sum + num**2
            # avg_squared = sum / len(channel_1)
            # vrms1.append(sqrt(avg_squared))
            # Vpp1.append(max(channel_1) - min(channel_1))

            fit_fun_1 = fit_sin(time1, data1, frequency)
            Vpp1.append(2 * fit_fun_1["amp"])
            vrms1.append(0.3535 * 2 * fit_fun_1["amp"])

        if (cfg.channel_2.active):
            avg_squared = 0
            sum = 0
            proc_data = get_num_wavelengths(frequency,time,channel_2,5)


            time2 = proc_data["time"]
            data2 = proc_data["data"]

            if (frequency == 216000):
                figure()
                plt.plot(time2,data2,time,channel_2)
                plt.show()
            # for num in data2: sum = sum + num ** 2
            # avg_squared = sum / len(data2)
            # vrms2.append(sqrt(avg_squared))
            # Vpp2.append(max(data2) - min(data2))

            fit_fun_2 = fit_sin(time2,data2,frequency)
            Vpp2.append(2*fit_fun_2["amp"])
            vrms2.append(0.3535*2*fit_fun_2["amp"])

        if (cfg.channel_3.active):
            avg_squared = 0
            sum = 0

            proc_data = get_num_wavelengths(frequency, time, channel_3, 5)
            time3 = proc_data["time"]
            data3 = proc_data["data"]

            # for num in channel_3: sum = sum + num ** 2
            # avg_squared = sum / len(channel_3)
            # vrms3.append(sqrt(avg_squared))
            # Vpp3.append(max(channel_3) - min(channel_3))

            fit_fun_3 = fit_sin(time3, data3, frequency)
            Vpp3.append(2 * fit_fun_3["amp"])
            vrms3.append(0.3535 * 2 * fit_fun_3["amp"])

        if (cfg.channel_4.active):
            avg_squared = 0
            sum = 0

            proc_data = get_num_wavelengths(frequency, time, channel_4, 5)
            time4 = proc_data["time"]
            data4 = proc_data["data"]

            # for num in channel_4: sum = sum + num ** 2
            # avg_squared = sum / len(channel_4)
            # vrms4.append(sqrt(avg_squared))
            # Vpp4.append(max(channel_4) - min(channel_4))

            fit_fun_4 = fit_sin(time4, data4, frequency)
            Vpp4.append(2 * fit_fun_4["amp"])
            vrms4.append(0.3535 * 2 * fit_fun_4["amp"])

    # Write the vrms values to an output file
    i = 0
    for freq in FREQ_LIST:
        outputStr = "{} \t {} \t {} \t {} \t {}\n".format(str(int(freq)),
                                                      str(vrms1[i]) if cfg.channel_1.active else "-",
                                                      str(vrms2[i]) if cfg.channel_2.active else "-",
                                                      str(vrms3[i]) if cfg.channel_3.active else "-",
                                                      str(vrms4[i]) if cfg.channel_4.active else "-")
        outputFile.write(outputStr)
        i += 1

    figure()

    plt.title('Siggen Vrms vs frequency')
    plt.hold

    reference_bool = False

    legend_list = []

    if(reference_bool):
        plt.plot(FREQ_LIST,vrms_siggen)
        legend_list.append("vrms_siggen")

    if (cfg.channel_1.active):
        plt.plot(FREQ_LIST,vrms1)
        #plt.plot(FREQ_LIST, Vpp1)
        legend_list.append(("Channel_1"))

    if (cfg.channel_2.active):
        plt.plot(FREQ_LIST,vrms2)
        #plt.plot(FREQ_LIST, Vpp2)
        legend_list.append("Channel_2")

    if (cfg.channel_3.active):
        plt.plot(FREQ_LIST,vrms3)
        #plt.plot(FREQ_LIST, Vpp3)
        legend_list.append("Channel_3")

    if (cfg.channel_4.active):
        plt.plot(FREQ_LIST,vrms4)
        #plt.plot(FREQ_LIST, Vpp4)
        legend_list.append("Channel_4")

    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Vrms Voltage (V)')
    plt.legend(legend_list)


    figure_name = home + "/" + cfg.temp_location + "/" + cfg.data_folder + "/Postprocessing/" + cfg.measurement_name
    #print(figure_name)
    plt.savefig(figure_name,format='png')

    plt.show()

    #Copy the YAML config file used to the directory where the data is
    print("cp {} {}config_file.yaml".format(config_file,home + "/" + cfg.temp_location + "/" + cfg.data_folder + "/Postprocessing/"))
    os.system("cp {} {}config_file.yaml".format(config_file,home + "/" + cfg.temp_location + "/" + cfg.data_folder + "/Postprocessing/"))

main()
