#!/usr/bin/env python3
import curses
from time import sleep
from lib.rig import Rig
#r = Rig('/dev/cu.usbserial-A7007QSU')
def cal(r):
    #f = 3000
    r.GRaw('G28 X0 Y0 Z0')
     
    # get the curses screen window
    screen = curses.initscr()
     
    # turn off input echoing
    curses.noecho()
     
    # respond to keys immediately (don't wait for enter)
    curses.cbreak()
     
    # map arrow keys to special values
    screen.keypad(True)
  
    print (screen.getbegyx())
    print (screen.getmaxyx())
    x, y, z = 0, 0, 0
    u, v, w = 20, 20, 1
    start = (-1, -1, -1)
    end = (-1, -1, -1)


    try:
        while True:
            screen.erase()
            if start == (-1, -1, -1):
                screen.addstr(0, 0, 'Start point: x, y, z = %d, %d, %d' % (x, y, z))
                screen.addstr(1, 0, 'Press Enter to select this point as start point.')
               
            else:
                screen.addstr(0, 0, 'End point: x, y, z = %d, %d, %d' % (x, y, z))
                screen.addstr(1, 0, 'Press Enter to select this point as end point.')
            char = screen.getch()
            if char == ord('q') or char == ord('\n'):
                if start == (-1, -1, -1):
                    start = (x, y, z)
                else:
                    end = (x, y, z)
                    break
            elif char == curses.KEY_RIGHT:
                # print doesn't work with curses, use addstr instead
                x += u
            elif char == curses.KEY_LEFT:
                x -= u
            elif char == curses.KEY_UP:
                y += v
            elif char == curses.KEY_DOWN:
                y -= v
            elif char == ord('w'):
                z += w
            elif char == ord('s'):
                z -= w
            
            #r.GRaw('G00 X%d Y%d Z%d F%d' % (x, y, z, f))
            r.GRaw('G00 X%d Y%d Z%d' % (x, y, z,))
            #sleep(0.01)
            r.GRaw('M400')
        print('Finished!')
        #r.close()
    finally:
        # shut down cleanly
        curses.nocbreak(); screen.keypad(0); curses.echo()
        curses.endwin()
    return (start, end)
