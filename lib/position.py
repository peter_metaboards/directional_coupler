

class Position:

    def __init__(self, xPos=0, yPos=0, zPos=0):
        self.xPos = xPos
        self.yPos = yPos
        self.zPos = zPos
        return

######################################################
######################################################    

    def SetPosition(self, xPos=0, yPos=0, zPos=0):
        self.xPos = xPos
        self.yPos = yPos
        self.zPos = zPos
        return
#
#####################################################
######################################################    

    def ShowPosition(self):
        positionString = "X:" + str(self.xPos) + " - Y:" + str(self.yPos) + " - Z:" + str(self.zPos)
        print(positionString)
        return

######################################################
######################################################  

    def SetXPosition(self,xPos):
        self.xPos = xPos
        return

######################################################
######################################################

    def SetYPosition(self,yPos):
        self.yPos = yPos
        return

######################################################
######################################################    

    def SetZPosition(self,zPos):
        self.zPos = zPos
        return 

######################################################
######################################################

    def GetXPosition(self):
        return self.xPos

######################################################
######################################################

    def GetYPosition(self):
        return self.yPos

######################################################
######################################################

    def GetZPosition(self):
        return self.zPos
