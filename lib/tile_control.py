import serial

# THIS TALKS TO THE ARDUINO OVER SERIAL
# TURNS TILES ON AND OFF
# RESET IT

class Control:
    def __init__(self, port, width, height, mapping):
        ser = serial.Serial()
        ser.baudrate = 9600   # XY rig
        ser.port = port
        ser.bytesize  = 8
        ser.parity    = 'N'
        ser.stopbits  = 1
        ser.timeout   = 0.5       # Read timeout, in seconds
        ser.xonxoff   = 0
        ser.rtscts    = 0
        ser.dsrdtr    = 0       # Essential for MKS Gen controller
        self.width, self.height = width, height
        self.ser = ser
        self.ser.open()
        self.mapping = mapping
            
        
    def command(self, cmd):
        self.ser.write(bytes(cmd, "ascii"))
    
    def reset(self):
        self.command("r")

    def turn_tile_coords(self, x, y, mode):
        self.turn_tile(self.mapping[x+y*self.width],mode   )
    def turn_tile(self, tile_id, mode):
        self.command("%d %d\n" % (tile_id, 1 if mode else 0))
		
# THIS THE THE PATH PART - ROW MAJORING [0,1] etc
        
    def do_path(self, path):
        for path_i, control_i in self.mapping.items():
            if path & (1 << path_i):
                self.turn_tile(control_i, False)
            else:
                self.turn_tile(control_i, True)
