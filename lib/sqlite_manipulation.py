import sys, getopt, os, time
import skrf
import sqlite3
from skrf import Network
import numpy as np
import math


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# Converts s-parameters to complex values

def sql2complex(sqlout):
	s_array = np.zeros(len(sqlout),np.complex128)
	for f in range(len(sqlout)):
		re_Z = sqlout[f][0]/((1+np.tan(sqlout[f][1])**2)**0.5)
		im_Z = re_Z*np.tan(sqlout[f][1])
		s_array[f] = re_Z+1j*im_Z
	return s_array

def sql2dB(sqlout):
    m_array = np.zeros(len(sqlout),np.float)
    p_array = np.zeros(len(sqlout),np.float)
    for f in range(len(sqlout)):
        m_array[f] = 20*np.log10(sqlout[f][0])
        p_array[f] = sqlout[f][1]*180/math.pi
    return m_array,p_array

def s2p_complex(sp_s11,sp_s12,sp_s21,sp_s22):
    sps = np.ndarray([len(sp_s11),2,2], dtype=np.complex)
    sps[:,0,0] = sql2complex(sp_s11)
    sps[:,0,1] = sql2complex(sp_s12)
    sps[:,1,0] = sql2complex(sp_s21)
    sps[:,1,1] = sql2complex(sp_s22)
    return sps

    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# SQL read of file

def read_sql_freq(filename):
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    c.execute('''SELECT `frequency (Hz)` FROM Sparameters''')
    fs = np.unique(c.fetchall())
    return fs

def read_sql_freq_table(filename,table):
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    c.execute('''SELECT `frequency (Hz)` FROM {:s}'''.format(table))
    fs = np.unique(c.fetchall())
    return fs

def read_sql_coords(filename,axis):
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    c.execute('''SELECT '''+axis+''' FROM Sparameters''')
    coords =  np.unique(c.fetchall())
    return coords

def read_sql_coords_table(filename,table,axis):
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    c.execute('''SELECT '''+axis+''' FROM {:s}'''.format(table))
    coords =  np.unique(c.fetchall())
    return coords
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# SQL read of file

def read_sql_sp(filename, fs, **kwargs):    
    conn = sqlite3.connect(filename)
    c = conn.cursor()
    x = kwargs.get('x', None)
    y = kwargs.get('y', None)
	
    if kwargs is not None:
        c.execute('''SELECT `mag(S11)`,`rad(S11)` FROM Sparameters WHERE X = ? and Y = ?''', (x,y))
        sp_s11 = c.fetchall()
        c.execute('''SELECT `mag(S12)`,`rad(S12)` FROM Sparameters WHERE X = ? and Y = ?''', (x,y))
        sp_s12 = c.fetchall()
        c.execute('''SELECT `mag(S21)`,`rad(S21)` FROM Sparameters WHERE X = ? and Y = ?''', (x,y))
        sp_s21 = c.fetchall()
        c.execute('''SELECT `mag(S22)`,`rad(S22)` FROM Sparameters WHERE X = ? and Y = ?''', (x,y))
        sp_s22 = c.fetchall()
    else:
        c.execute('''SELECT `mag(S11)`,`rad(S11)` FROM Sparameters''')
        sp_s11 = c.fetchall()
        c.execute('''SELECT `mag(S12)`,`rad(S12)` FROM Sparameters''')
        sp_s12 = c.fetchall()
        c.execute('''SELECT `mag(S21)`,`rad(S21)` FROM Sparameters''')
        sp_s21 = c.fetchall()
        c.execute('''SELECT `mag(S22)`,`rad(S22)` FROM Sparameters''')
        sp_s22 = c.fetchall()
    return sp_s11,sp_s12,sp_s21,sp_s22

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def gen_ntwk(filename, fs, vna, **kwargs):
    
    # NB: VNA attached to both ports - parallel load NOT the load at the junction! 
    # hence: z0=[vna,vna] NOT z0 =[vna,probe] as previous versions had
    # Note that a Nework.Frequency object was used above, NOT frequencies as pulled from SQLite
    # This is to ensure that frequency unit is defined - defualts to GHz!
        
    xc = kwargs.get('xc', None)
    yc = kwargs.get('yc', None)
    fv = skrf.Frequency.from_f(fs,unit='hz')
    
    if kwargs is not None:
        s11,s12,s21,s22 = read_sql_sp(filename, fs, x=xc, y=yc)
    else:
	    s11,s12,s21,s22 = read_sql_sp(filename, fs)	

    sps = s2p_complex(s11,s12,s21,s22)
    ntwk = skrf.Network(frequency=fv,s=sps,z0=[vna,vna])
    return ntwk

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def gen_res(R,fs,vna):
    fv = skrf.Frequency.from_f(fs,unit='hz')
    zps = R*np.ones([len(fs),2,2],dtype=complex)
    sps = np.zeros([len(fs),2,2],dtype=complex)
    sps[:,0,0] = ((zps[:,0,0]-vna)*(zps[:,1,1]+vna)-zps[:,0,1]*zps[:,1,0])/((zps[:,0,0]+vna)*(zps[:,1,1]+vna)-zps[:,0,1]*zps[:,1,0])
    sps[:,0,1] = (2*zps[:,0,1]*vna)/((zps[:,0,0]+vna)*(zps[:,1,1]+vna)-zps[:,0,1]*zps[:,1,0])
    sps[:,1,0] = (2*zps[:,1,0]*vna)/((zps[:,0,0]+vna)*(zps[:,1,1]+vna)-zps[:,0,1]*zps[:,1,0])
    sps[:,0,0] = ((zps[:,0,0]+vna)*(zps[:,1,1]-vna)-zps[:,0,1]*zps[:,1,0])/((zps[:,0,0]+vna)*(zps[:,1,1]+vna)-zps[:,0,1]*zps[:,1,0])    
    res_ntwk = skrf.Network(frequency=fv,s = sps,z0=[vna,vna])
    return res_ntwk

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


def gen_s2p(ntwk,outfile):
    # To save on run time - checks to see whether the '.s2p' files already exist
    if (os.path.isfile(outfile)==False):
        ntwk.write(outfile)	
        b = skrf.Network(outfile)
    return

