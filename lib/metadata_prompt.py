def defineConditions():

    print("What is the tile version? \n (0) new \n (1) 0.1 \n (2) 0.22 \n (3) 0.31 \n (4) 0.32 \n (5) 0.33 \n (6)0.36 \n (7) 0.36 \n (8) 0.41 \n (9) 0.4a1")
    while True:
        try:
            inp = input().strip()
            if inp in ['1','2','3','4','5','6','7','8','9']:
                tileVersion = tileVersionFunc(inp)
                break
            if inp in ['0']:
                tileVersion = input().strip()
                print(tileVersion)
                break
        except:
            print ('That was not one of the options.')
            continue

    print("What is the surface? \n (0) other \n (1) Wood \n (2) Plastic \n (3) Metal \n (4) Glass")
    while True:
        try:
            inp = input().strip()
            if inp in ['1','2','3','4']:
                surface = surfaceFunc(inp)
                break
            if inp in ['0']:
                surface = input().strip() 					
                break			
        except:
            print ('That was not one of the options.')
            continue

    print("What is the substrate? \n (0) other \n (1) Impregnated paper (FR-2) \n (2) Fiberglass (FR-4) \n (3) Dielectic Plastic \n (4) FLEX \n (5) Ceramic")
    while True:
        try:
            inp = input().strip()
            if inp in ['1','2','3','4','5']:
                substrate = substrateFunc(inp)
                break
            if inp in ['0']:
                substrate = input().strip() 									
                break				
        except:
            print ('That was not one of the options.')
            continue

    print("Is ferrite present? (y/n)")
    while True:
        try:
            inp = input().strip()
            if inp in ['y','Y','n','N']:
                ferrite = ferriteFunc(inp)

                batch_desc = "Tile Version = " + tileVersion + "\n" + "Surface = " + surface + "\n" + "Substrate = " + substrate + "\n" + "Ferrite = " + ferrite

                if inp in ['y','Y']:
                    print ('Tile distance above ferrite (mm) (+ve is above)')
                    try:
                        inp = input().strip()
                        ferriteHeight = int(inp)

                        batch_desc = "Tile Version = " + tileVersion + "\n" + "Surface = " + surface + "\n" + "Substrate = " + substrate + "\n" + "Ferrite = " + ferrite + "\n" + "Ferrite height (mm) = " + str(ferriteHeight)
                        break
                    except:
                        print ('That was not a recognised batch name.')
                        continue

                break
        except:
            print ('That was not a recognised batch name.')
            continue


        print(("Using the following options: \n" + batch_desc))

    return batch_desc

def tileVersionFunc(x):
    return {
        '1':'0.10',
        '2':'0.22',
        '3':'0.31',
        '4':'0.32',
        '5':'0.33',
        '6':'0.35',
        '7':'0.36',
        '8':'0.41',
        '9':'0.4a1'
        }[x]

def surfaceFunc(x):
    return {
        '1':'Wood',
        '2':'Plastic',
        '3':'Metal',
        '4':'Glass'
        }[x]

def substrateFunc(x):
    return {
        '1':'Impregnated paper (FR-2)',
        '2':'Fiberglass (FR-4)',
        '3':'Dielectric plastic (RF)',
        '4':'FLEX',
        '5':'Ceramic'
        }[x]

def ferriteFunc(x):
    return {
        'y':'Yes',
        'n':'No',
        'Y':'Yes',
        'N':'No'
        }[x]