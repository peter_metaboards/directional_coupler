import sys
import time
import telnetlib
from lib.position import Position

# Z : Positive -> upwards
# Y : Positive -> To The wall
# X : Positive -> to the Right(door)

class Rig:

    def __init__(self, host="192.168.1.150", port=23):
        self.host = host
        self.port = port
        self.tileSize = 104

        self.minX = 0
        self.minY = 0
        self.minZ = 0
        self.maxX = 710
        self.maxY = 710
        self.maxZ = 140

        self.position = Position()

        tn = telnetlib.Telnet(host,port)
        self.tn = tn

        self.GRaw('')

        while 1:
            response = tn.read_until(b"\n").decode().rstrip()
            print (response)
            if response == '> ok':
                #print(response)
                return

            elif response == 'ok':
                return

            elif response == 'Smoothie command shell':
                return

######################################################
######################################################

    def AutoConfig(self):
        "Sets up the rig with the desired settings"
        output = 'G90'          # Set the machine to absolute coordinates
        self.GRaw(output)

        output = 'G1' + ' ' + 'F10000'
        self.GRaw(output)

        output = 'G28'          # Home the machine
        self.GRaw(output)

        self.position = Position(0,0,0)
        return

######################################################
######################################################

    def AutoTest(self):
        "Tests the rig with movement commands"
        self.G1XYZ(100,100,0)

        self.G1XYZ(700,100,10)

        self.G1XYZ(700,700,20)

        self.G1XYZ(100,700,10)

        self.G1XYZ(100,100,0)

        self.G1XYZ(300,300,0)

        self.G1XYZ(0,0,0)

        return

######################################################
######################################################

    def HomeRig(self):
        "Home the rig"
        output = 'G28'          # Home the machine
        self.GRaw(output)
        return

######################################################
######################################################

    def GRaw(self, string):
        "Outputs a string to the ethernet, waits for response"
        tn = self.tn

        stringChecked = self.checkSafeCommand(string)

        tn.write(stringChecked.encode() + b'\n')

        response = ''

        while 1:
            response = tn.read_until(b'\n').decode().rstrip()
            #print (response)
            if response == '> ok':
                return 1

            elif response == 'ok':
                return 1

            elif response[0:2] == 'ok':
                return 1

            elif response[0:3] == '> ok':
                return 1

######################################################
######################################################

    def GRawReturn(self, string):
        "Outputs a string to the ethernet, waits for response"
        tn = self.tn

        stringChecked = self.checkSafeCommand(string)

        tn.write(stringChecked.encode() + b'\n')

        response = ''

        while 1:
            response = tn.read_until(b'\n').decode().rstrip()
            #print (response)
            if response == '> ok':
                return response

            elif response == 'ok':
                return response

            elif response[0:2] == 'ok':
                return response

            elif response[0:3] == '> ok':
                return response

######################################################
######################################################

    def G10(self, X, Y, Z=0, F=0):
        "Outputs a G1 command, waits for response"
        output = 'G1' + 'X' + str(X) + 'Y' + str(Y) + 'Z' + str(Z)
        if F != 0:
            output += 'F' + str(F)
        self.GRaw(output)
        return

######################################################
######################################################

    def G1XY(self, X, Y, F=0):
        "Outputs a G1 command, waits for response"
        output = 'G1' + ' ' + 'X' + str(X) + ' ' + 'Y' + str(Y) + ' '
        if F != 0:
            output += 'F' + str(F)
        self.GRaw(output)
        return

######################################################
######################################################

    def G1Z(self, Z):
        "Outputs a G1 command, waits for response"
        output = 'G1' + ' ' + 'Z' + str(Z)
        self.GRaw(output)
        return

######################################################
######################################################

    def Feed(self, F):
        "Outputs a G1 command, waits for response"
        output = 'G1' + ' ' + 'F' + str(F)
        self.GRaw(output)
        return

######################################################
######################################################

    def G1XYZ(self, X, Y, Z):
        "Outputs a G1 command, waits for response"
        output = 'G1' + ' ' + 'X' + str(X) + ' ' + 'Y' + str(Y) + ' ' + 'Z' + str(Z) + ' '
        self.GRaw(output)
        return

######################################################
######################################################

    def GetPosition(self):
        "Returns the current position"
        output = 'M114'          # Get Current position the machine
        string = self.GRawReturn(output)
        stringArray = string.split()

        xPos = float(stringArray[2].split(":")[1])
        yPos = float(stringArray[3].split(":")[1])
        zPos = float(stringArray[4].split(":")[1])

        self.position.SetPosition(xPos,yPos,zPos)
        #self.position.ShowPosition()
        return self.position


######################################################
######################################################

    def SetTileSize(self,tileSize = 104):
        self.tileSize = tileSize
        return

######################################################
######################################################

    def GetTileSize(self):
        return self.tileSize

######################################################
######################################################

    def AdvanceX(self,direction = 1):
        "Advance the X by 1 tile in the X direction"
        self.GetPosition()

        xPos = self.position.GetXPosition()

        if direction == 1:

            newXPos = xPos + self.GetTileSize()
            print(newXPos)

        elif direction == -1:

            newXPos = xPos - self.GetTileSize()
            print(newXPos)

        output = 'G1' + ' ' + 'X' + str(newXPos)          # Home the machine
        print(output)
        self.GRaw(output)

        return

######################################################
######################################################

    def AdvanceY(self,direction = 1):
        "Advance the X by 1 tile in the X direction"
        self.GetPosition()

        yPos = self.position.GetYPosition()

        if direction == 1:

            newYPos = yPos + self.GetTileSize()
            print(newYPos)

        elif direction == -1:

            newYPos = yPos - self.GetTileSize()
            print(newYPos)

        output = 'G1' + ' ' + 'Y' + str(newYPos)          # Home the machine
        print(output)
        self.GRaw(output)

        return

######################################################
######################################################

    def checkSafeCommand(self,command):

        stringArray = command.split()

        counter = 0

        for item in stringArray:

            if item.find('X') == 0:
                xPos = float(item.lstrip('X'))

                if xPos > self.maxX:
                    stringArray[counter] = "X" + str(self.maxX)
                elif xPos < self.minX:
                    stringArray[counter] = "X" + str(self.minX)

            if item.find('Y') == 0:
                yPos = float(item.lstrip('Y'))

                if yPos > self.maxY:
                    stringArray[counter] = "Y" + str(self.maxY)
                elif yPos < self.minY:
                    stringArray[counter] = "Y" + str(self.minY)

            if item.find('Z') == 0:
                zPos = float(item.lstrip('Z'))

                if zPos > self.maxZ:
                    stringArray[counter] = "Z" + str(self.maxZ)
                elif zPos < self.minZ:
                    stringArray[counter] = "Z" + str(self.minZ)

            counter = counter + 1

        newCommand = ''
        counter = 0

        for item in stringArray:
            newCommand += stringArray[counter]
            newCommand += ' '
            counter = counter + 1

        return newCommand


#######################################################
#######################################################

    def checkSafeCommandDebug(self,command):

        stringArray = command.split()
        print(stringArray)
        counter = 0

        for item in stringArray:

            if item.find('X') == 0:
                xPos = float(item.lstrip('X'))
                xPosStr = str(xPos)
                print(('xPos =' + xPosStr))

                if xPos > self.maxX:
                    stringArray[counter] = "X" + str(self.maxX)
                elif xPos < self.minX:
                    stringArray[counter] = "X" + str(self.minX)

            if item.find('Y') == 0:
                yPos = float(item.lstrip('Y'))

                if yPos > self.maxY:
                    stringArray[counter] = "Y" + str(self.maxY)
                elif yPos < self.minY:
                    stringArray[counter] = "Y" + str(self.minY)

            if item.find('Z') == 0:
                zPos = float(item.lstrip('Z'))

                if zPos > self.maxZ:
                    stringArray[counter] = "Z" + str(self.maxZ)
                elif zPos < self.minZ:
                    stringArray[counter] = "Z" + str(self.minZ)

            counter = counter + 1

        newCommand = ''
        counter = 0

        for item in stringArray:
            newCommand += stringArray[counter]
            newCommand += ' '
            counter = counter + 1

        print(newCommand)

        return newCommand

######################################################
######################################################

##    def Flush(self):
##        "Flush data from input buffer"
##        LastCom = time.time()
##        tn = self.tn
##        while 1:
##            if ser.inWaiting > 0:
##                response = ser.readline().decode().rstrip()
##                print(response)
##                if response != 'wait':
##                    LastCom = time.time()
##            if time.time() > LastCom + 1.2:
##                return

######################################################
######################################################

    def connect(self):
        try:
            print('Connecting...')
            self.tn.open(self.host,self.port)
        except:
            print('Failed to open serial port')
            sys.exit()
        time.sleep(1)
        self.Flush()

######################################################
######################################################

    def close(self):
        print('Finished')
        self.tn.close()

######################################################
######################################################

    def __del__(self):
        self.close()

