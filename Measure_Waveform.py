import sys, os, sqlite3, subprocess, getopt, math, argparse
import time as time
from os.path import expanduser
from rigolFG import RigolFunctionGenerator
from ds1054z import DS1054Z
import matplotlib.pyplot as plt
from config.Measure_Waveform_Config import measure_waveform_config_from_yaml
""" 
This script automates the characterisation of the directional coupler. 
The script scans the signal generator over different frequencies and VRms values and saves them into a database file
The scope saves the Vrms value for the transmitted and reflected waves.
It stops to allow you to make changes to the setup each time.
Measurements taken are:
Load, open, short, coil
Each component is connected directly to the output port of the directional coupler
"""

####################################################################################################
####################################################################################################

""" Global Variables """

# Useful variables
ON = 1
OFF = 0

SCOPE_TIME_FACTORS = [1, 2, 5, 10, 20, 50, 100, 200, 500]
SCOPE_VOLTAGE_FACTORS = [1, 2, 5, 10, 20, 50, 100, 200, 500]
SCOPE_TIME_BASE_LIST = []
SCOPE_VOLTAGE_LIST = []
FREQ_LIST = []

####################################################################################################
####################################################################################################
#Helper functions

def parser():
    """Perform an arm sweep described by a YAML config file"""
    parser = argparse.ArgumentParser()
    parser.add_argument("config_file", help="YAML config file to read from")
    parser.add_argument(
        "--override", action="store_true", help="Replace the output database if it already exists"
    )

    args = parser.parse_args()
    cfg = measure_waveform_config_from_yaml(args.config_file)

    return [cfg,args.config_file]

def connectToSiggen(cfg):
    # Connect to the siggen
    sigGen = RigolFunctionGenerator(host=cfg.siggen.siggen_ip)
    sigGen.reset()
    return sigGen

def setupSiggen(sigGen,cfg):

    sigGen.setFrequency(1,cfg.frequency.low)
    sigGen.setOutput(ON)
    sigGen.setOutputLoad(cfg.siggen.impedance_val)
    sigGen.setVoltageUnit(cfg.siggen.voltage_mode)

    if(cfg.siggen.voltage_min != -1):
        vrmsTemp = range(int(cfg.siggen.voltage_min * 10), int((cfg.siggen.voltage_max + 0.1) * 10), int(cfg.siggen.voltage_step* 10))
        vrmsList = []
        for item in vrmsTemp:
            vrmsList.append(item / 10)
        sigGen.setVoltage(cfg.siggen.voltage_min)
    else:
        sigGen.setVoltage(cfg.siggen.voltage)

    if(cfg.frequency.step == -1):
        num_points = cfg.frequency.num_points
        freqStep = (cfg.frequency.high - cfg.frequency.low) / cfg.frequency.num_points
        FREQ_LIST.append(cfg.frequency.low)
        temp = cfg.frequency.low
    else:
        num_points = round((cfg.frequency.high - cfg.frequency.low) / cfg.frequency.step)
        freqStep = cfg.frequency.step
        FREQ_LIST.append(cfg.frequency.low)
        temp = cfg.frequency.low

    for i in range(1, num_points + 1, 1):
        temp = int(temp + freqStep)
        FREQ_LIST.append(temp)
        print(temp)


def connectToScope(cfg):
    # Connect to the scope
    scope = DS1054Z(cfg.scope.scope_ip)
    print(scope.idn + "\n")
    return scope


def setupScope(scope,cfg):

    #Define useful lists of all of the scope voltage scale presets
    for item in SCOPE_TIME_FACTORS:
        SCOPE_TIME_BASE_LIST.append(item * 1e-9)
        SCOPE_TIME_BASE_LIST.append(item * 1e-6)
        SCOPE_TIME_BASE_LIST.append(item * 1e-3)

    for item in SCOPE_VOLTAGE_FACTORS:
        SCOPE_VOLTAGE_LIST.append(item)
        SCOPE_VOLTAGE_LIST.append(item * 1e-3)

    #Setup all of the channels we need as set in the config file
    if (cfg.channel_1.active):
        scope.display_channel(1,cfg.channel_1.active)
        scope.set_channel_scale(1, cfg.channel_1.channel_scale, False)
        scope.set_probe_ratio(1, cfg.channel_1.probe_scale)
    else:
        scope.display_channel(1, 0)

    if (cfg.channel_2.active):
        scope.display_channel(2,cfg.channel_2.active)
        scope.set_channel_scale(2, cfg.channel_2.channel_scale, False)
        scope.set_probe_ratio(2, cfg.channel_2.probe_scale)
    else:
        scope.display_channel(2, 0)

    if (cfg.channel_3.active):
        scope.display_channel(3,cfg.channel_3.active)
        scope.set_channel_scale(3, cfg.channel_3.channel_scale, False)
        scope.set_probe_ratio(3, cfg.channel_3.probe_scale)
    else:
        scope.display_channel(3, 0)

    if (cfg.channel_4.active):
        scope.display_channel(4,cfg.channel_4.active)
        scope.set_channel_scale(4,cfg.channel_4.channel_scale,False)
        scope.set_probe_ratio(4, cfg.channel_4.probe_scale)
    else:
        scope.display_channel(4, 0)

    # Set the scope time base mode
    timeBaseModeStr = ":Timebase:mode {}".format(cfg.scope.time_base_mode)
    scope.write(timeBaseModeStr)

    # Quickly run the scope so that we can change the number of samples
    scope.run()
    time.sleep(3)

    scope.memory_depth = cfg.scope.memory_depth
    time.sleep(1)
    scope.timebase_scale = cfg.scope.time_base

    scope.run()
    time.sleep(0.6)

    if (cfg.channel_1.active):
        checkVerticalRange(scope, 1)
    if (cfg.channel_2.active):
        checkVerticalRange(scope, 2)
    if (cfg.channel_3.active):
        checkVerticalRange(scope, 3)
    if (cfg.channel_4.active):
        checkVerticalRange(scope, 4)

    scope.stop()


def checkTimeBase(scope,freq):
    scopeTimeBaseTemp = (1 / (freq/3))
    tempVal = 10000
    for item in SCOPE_TIME_BASE_LIST:
        residual = abs(scopeTimeBaseTemp - item)
        if (residual < tempVal):
            tempVal = residual
            scopeTimeBase = item

    scope.timebase_scale = scopeTimeBase

def checkVerticalRange(scope,channelNo):
    #print(channelNo)
    rangeChanged = True
    while rangeChanged:
        VRange = (scope.get_channel_scale(channelNo))
        V_meas = scope.get_channel_measurement(channelNo, "vpp", "current")

        if(V_meas == None):
            scope.run()
            while True:
                VRange = (scope.get_channel_scale(channelNo))
                V_meas = scope.get_channel_measurement(channelNo, "vpp", "current")
                print(V_meas)
                if(V_meas == None):
                    tempVal = 10000
                    for item in SCOPE_VOLTAGE_LIST:
                        residual = abs((VRange * 2) - item)
                        if (residual < tempVal):
                            tempVal = residual
                            NewVRange = item
                    print("{},{}".format(VRange,NewVRange))

                    scope.set_channel_scale(channelNo, NewVRange, True)
                    time.sleep(0.5)
                    print("Decreasing range to get data")
                else:
                    break
                #time.sleep(0.2)
            time.sleep(0.5)
        if(V_meas > (7*VRange)):
            scope.run()
            rangeChanged = True
            tempVal = 10000
            for item in SCOPE_VOLTAGE_LIST:

                residual = abs((VRange*1.1) - item)
                if(residual<tempVal):
                    tempVal = residual
                    NewVRange = item
            print("Reducing range to prevent clipping")

            scope.set_channel_scale(channelNo,VRange*1.1,False)
            time.sleep(0.5)

        elif((V_meas < (1.5*VRange)) and (VRange != 1e-3)):
            scope.run()
            scaleFactor = 2

            if(V_meas == 0):
                scaleFactor = 4

            elif (V_meas / VRange) < 1 :
                scaleFactor = 2

            rangeChanged = True
            tempVal = 10000

            simpleVal = (VRange / (scaleFactor))

            # for item in scopeVoltageList:
            #
            #     residual = abs((VRange / (2**scaleFactor)) - item)
            #     if (residual < tempVal):
            #         tempVal = residual
            #         NewVRange = item
            print("Increasing range to ensure good resolution")
            scope.set_channel_scale(channelNo, simpleVal, False)
            time.sleep(0.5)
        else:
            #print("No changes made")
            rangeChanged = False


def main():

    [cfg,config_file] = parser()

    print(cfg)

    sigGen = connectToSiggen(cfg)

    setupSiggen(sigGen,cfg)

    scope = connectToScope(cfg)

    setupScope(scope,cfg)

    sigGen.setVoltage(cfg.siggen.voltage)

    # output file base name
    outputFileBaseName = cfg.measurement_name
    home = expanduser("~")
    print(home+"/"+cfg.temp_location)

    if not os.path.exists(home + "/" + cfg.temp_location + "/" + cfg.data_folder):
        os.chdir(home + "/" + cfg.temp_location)
        os.mkdir(cfg.data_folder)

    os.chdir(home + "/" + cfg.temp_location + "/" + cfg.data_folder)

    if not os.path.exists(home + "/" + cfg.temp_location + "/" + cfg.data_folder + "/Postprocessing"):
        os.chdir(home + "/" + cfg.temp_location + "/" + cfg.data_folder)
        os.mkdir("Postprocessing")

    #Copy the YAML config file used to the directory where the data is
    print("cp {} {}config_file.yaml".format(config_file,home + "/" + cfg.temp_location + "/" + cfg.data_folder + "/Postprocessing/"))
    os.system("cp {} {}config_file.yaml".format(config_file,home + "/" + cfg.temp_location + "/" + cfg.data_folder + "/Postprocessing/"))

    current_time = time.gmtime()


    for freq in FREQ_LIST:

        sigGen.setFrequency(1, freq)
        # Give the sigGen time to setup

        checkTimeBase(scope,freq)

        scope.run()
        time.sleep(0.6)

        # scope.single()
        # scope.tforce()
        # while scope.running:
        #     time.sleep(0.05)

        #Make sure the data can be read, isn't being clipped and has a good dynamic range
        # if(cfg.channel_1.active):
        #     checkVerticalRange(scope,1)
        # if (cfg.channel_2.active):
        #     checkVerticalRange(scope, 2)
        # if (cfg.channel_3.active):
        #     checkVerticalRange(scope, 3)
        # if (cfg.channel_4.active):
        #     checkVerticalRange(scope, 4)

        scope.stop()

       # time.sleep(0.2)

        if (cfg.channel_1.active):
            channel1 = scope.get_waveform_samples(1,'raw')

            while (len(channel1) == 0) or (channel1 == None):
                print("Recovering from 'None' data")
                scope.run()
                time.sleep(0.6)
                scope.stop()
                channel1 = scope.get_waveform_samples(1, 'raw')

        if (cfg.channel_2.active):
            channel2 = scope.get_waveform_samples(2, 'raw')

            while (len(channel2) == 0) or (channel2 == None):
                print("Recovering from 'None' data")
                scope.run()
                time.sleep(0.6)
                scope.stop()
                channel2 = scope.get_waveform_samples(2, 'raw')

        if (cfg.channel_3.active):
            channel3 = scope.get_waveform_samples(3, 'raw')

            while (len(channel3) == 0) or (channel3 == None):
                print("Recovering from 'None' data")
                scope.run()
                time.sleep(0.6)
                scope.stop()
                channel3 = scope.get_waveform_samples(3, 'raw')

        if (cfg.channel_4.active):
            channel4 = scope.get_waveform_samples(4, 'raw')

            while (len(channel4) == 0) or (channel4 == None):
                print("Recovering from 'None' data")
                scope.run()
                time.sleep(0.6)
                scope.stop()
                channel4 = scope.get_waveform_samples(4, 'raw')

        timeData = scope.waveform_time_values


        print("Frequency {}".format(freq))

        outputFileName = outputFileBaseName + "_" + str(int(freq))

        outputFile = open(outputFileName, 'w')
        outputFile.write("---\n")
        outputFile.write("Name: {}\n".format(cfg.measurement_name))
        outputFile.write("datetime: {}{}{}-{}:{}:{}\n\n".format(current_time.tm_year,current_time.tm_mon,current_time.tm_mday,current_time.tm_hour,current_time.tm_min,current_time.tm_sec))
        outputFile.write("scope: \n\t scope_ip:{} \n\t time_base:{} \n\t memory_depth:{} \n\t time_base_mode:{} \n\n"
                         .format(cfg.scope.scope_ip, cfg.scope.time_base, cfg.scope.memory_depth, cfg.scope.time_base_mode))
        outputFile.write("siggen: \n\t siggen_ip:{} \n\t voltage_mode:{} \n\t voltage:{} \n\t voltage_min:{} \n\t voltage_max:{} \n\t "
                         "voltage_step:{} \n\t impedance_val:{} \n\n".format(cfg.siggen.siggen_ip, cfg.siggen.voltage_mode,
                                                                        cfg.siggen.voltage,cfg.siggen.voltage_min,
                                                                        cfg.siggen.voltage_max,cfg.siggen.voltage_step,
                                                                        cfg.siggen.impedance_val))

        outputFile.write("frequency: \n\t low: {} \n\t high: {} \n\t step: {} \n\t num_points: {} \n\n".format(cfg.frequency.low,cfg.frequency.high,cfg.frequency.step,cfg.frequency.num_points))
        outputFile.write("Channel 1 connection: {}\n".format(cfg.data_header.channel_1_connection))
        outputFile.write("Channel 2 connection: {}\n".format(cfg.data_header.channel_2_connection))
        outputFile.write("Channel 3 connection: {}\n".format(cfg.data_header.channel_3_connection))
        outputFile.write("Channel 4 connection: {}\n\n".format(cfg.data_header.channel_4_connection))
        outputFile.write("Current_Frequency: {}\n\n".format(freq))
        outputFile.write("...\n")
        outputFile.write("\n\n")
        outputFile.write("Time \t Channel_1 \t Channel_2 \t Channel_3 \t Channel_4\n")
        i = 0

        print("Length of data is: {}".format(len(channel2)))

        # Assumes Channnel 1 is always active.
        for entry in timeData:
            outputStr = "{} \t{} \t{} \t{} \t{}\n".format(str(entry),
                                                     str(channel1[i]) if cfg.channel_1.active else "-",
                                                     str(channel2[i]) if cfg.channel_2.active else "-",
                                                     str(channel3[i]) if cfg.channel_3.active else "-",
                                                     str(channel4[i]) if cfg.channel_4.active else "-")
            outputFile.write(outputStr)
            i += 1



main()
