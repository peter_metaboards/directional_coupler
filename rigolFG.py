import vxi11
import time

"""
Wrapper class for the Rigol signal generator. This connects to a DG1022 signal generator via ethernet in the same way
the DS1054Z python library connects to the osciloscope. Work has been invested into breakout out a lot of the 
signal generator's functionality into functions which can be called in python as opposed to sending specific commands.
Also the signal generator doesn't allow remote control of channel two, so only channel one can controlled using
this python script. 
"""

class RigolFunctionGenerator:

    def __init__(self, host):

        self.device = vxi11.Instrument(host=host)

        print(self.device.ask("*IDN?"))

    def getID(self):
        print(self.device.ask("*IDN?"))

    ############################################################################################################################
    ############################################################################################################################
    #  Set methods
    # Frequency commands

    def setFrequency(self,channel,frequency):
        if(channel==1):
            self.write("frequency {:f}".format(frequency))
        elif(channel==2):
            self.write("frequency:CH2 {:f}".format(frequency))

    def setFrequencyStart(self,channel,startFrequency):
        if (channel == 1):
            self.write("frequency:start {:f}".format(startFrequency))
        elif (channel == 2):
            print("Only valid for channel 1")

    def setFrequencyStop(self,channel,stopFrequency):
        if (channel == 1):
            self.write("frequency:Stop {:f}".format(stopFrequency))
        elif (channel == 2):
            print("Only valid for channel 1")

    def setFrequencyCentre(self,channel,centreFrequency):
        if (channel == 1):
            self.write("frequency:center {:f}".format(centreFrequency))
        elif (channel == 2):
            print("Only valid for channel 1")

    def setFrequencySpan(self,channel,frequencySpan):
        if (channel == 1):
            self.write("frequency:span {:f}".format(frequencySpan))
        elif (channel == 2):
            print("Only valid for channel 1")


    #################################################################
    #################################################################
    #Siggen Function commands

    def setFunction(self,funcName):
        self.write("function {:s}".format(funcName))

    def setUserFunction(self,funcName):
        self.write("function:user {:s}".format(funcName))

    #################################################################
    #################################################################
    #Voltage commands

    def setVoltage(self,voltage):
        self.write("voltage {:f}".format(voltage))

    def setVoltageHigh(self,voltageHigh):
        self.write("voltage:high {:f}".format(voltageHigh))

    def setVoltageLow(self,voltageLow):
        self.write("voltage:low {:f}".format(voltageLow))

    def setVoltageOffset(self,voltageOffset):
        self.write("voltage:offset {:f}".format(voltageOffset))

    def setVoltageUnit(self,unit):
        self.write("voltage:unit {:s}".format(unit))

    #################################################################
    #################################################################
    # AM commands

    #################################################################
    #################################################################
    # FM commands

    #################################################################
    #################################################################
    # PM commands

    #################################################################
    #################################################################
    # Sweep commands

    def setSweepSpacing(self,type):
        self.write("sweep:spacing {:s}".format(type))

    def setSweetTime(self,time):
        self.write("sweep:time {:f}".format(time))

    def setSweepState(self,state):
        if(state):
            self.write("sweep:state on")
        elif(state):
            self.write("sweep:state off")
    #################################################################
    #################################################################
    #Output Commands

    def setOutput(self,state):
        if(state):
            self.write("output on")
        elif(state):
            self.write("output off")

    def setOutputLoad(self,loadVal):
        self.write("output:load {:f}".format(loadVal))

    def setOutputPolarity(self,polarity):
        if(polarity == "NORM"):
            self.write("output:polarity normal")
        elif(polarity == "INV"):
            self.write("output:polarity inverted")

    def setOutputSync(self,state):
        if (state):
            self.write("output:sync on")
        elif (state):
            self.write("output:sync off")

    def setOutputTrigger(self,state):
        if (state):
            self.write("output:trigger on")
        elif (state):
            self.write("output:trigger off")

    ############################################################################################################################
    ############################################################################################################################
    # Get methods
    #Frequency commands

    def getFrequency(self,channel):
        if (channel == 1):
            return self.ask("frequency?")
        elif (channel == 2):
            print("Only valid for channel 1")

    def getFrequencyStart(self,channel):
        if (channel == 1):
            return self.ask("frequency:start?")
        elif (channel == 2):
            print("Only valid for channel 1")

    def getFrequencyStop(self,channel):
        if (channel == 1):
            return self.ask("frequency:Stop?")
        elif (channel == 2):
            print("Only valid for channel 1")

    def getFrequencyCentre(self,channel):
        if (channel == 1):
            return self.ask("frequency:center?")
        elif (channel == 2):
            print("Only valid for channel 1")

    def getFrequencySpan(self,channel):
        if (channel == 1):
            return self.ask("frequency:span?")
        elif (channel == 2):
            print("Only valid for channel 1")

    #################################################################
    #################################################################
    # Siggen Function commands

    def getFunction(self):
        return self.ask("function?")

    def getUserFunction(self):
        return self.ask("function:user?")

    #################################################################
    #################################################################
    #Voltage commands

    def getVoltage(self):
        return self.ask("voltage?")

    def getVoltageHigh(self):
        return self.ask("voltage:high?")

    def getVoltageLow(self):
        return self.ask("voltage:low?")

    def getVoltageOffset(self):
        return self.ask("voltage:offset?")

    def getVoltageUnit(self):
        return self.ask("voltage:unit?")

    #################################################################
    #################################################################
    # AM commands

    #################################################################
    #################################################################
    # FM commands

    #################################################################
    #################################################################
    # PM commands

    #################################################################
    #################################################################
    # Sweep commands

    def getSweepSpacing(self):
        self.ask("sweep:spacing?")

    def getSweetTime(self):
        self.ask("sweep:time?")

    def getSweepState(self):
        self.ask("sweep:state?")

    #################################################################
    #################################################################
    #Output Commands

    def getOutput(self):
        return self.ask("output?")

    def getOutputLoad(self):
        return self.ask("output:load?")

    def getOutputPolarity(self):
        return self.ask("output:polarity?")

    def getOutputSync(self):
        return self.ask("output:sync?")

    def getOutputTrigger(self):
        return self.ask("output:trigger?")


    ############################################################################################################################
    ############################################################################################################################
    # Helper commands

    def write(self, command):
        """Send an arbitrary command directly to the scope"""
        self.device.write(command)
        time.sleep(0.2)
 
    def read(self, num=-1):
        """Read an arbitrary amount of data directly from the scope"""
        return self.device.read(num)



    def ask(self,command):
        self.device.write(command)
        time.sleep(0.2)
        return self.device.read()
 
    def reset(self):
        """Reset the instrument"""
        self.device.write("*RST")