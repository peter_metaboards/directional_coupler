import sys, os, sqlite3, subprocess, getopt, math, argparse, csv

from os.path import expanduser
import matplotlib.pyplot as plt
import numpy, scipy.optimize, scipy.signal
from pylab import *
from config.Measure_Waveform_Config import measure_waveform_config_from_yaml
""" 
This script automates the characterisation of the directional coupler. 
The script scans the signal generator over different frequencies and VRms values and saves them into a database file
The scope saves the Vrms value for the transmitted and reflected waves.
It stops to allow you to make changes to the setup each time.
Measurements taken are:
Load, open, short, coil
Each component is connected directly to the output port of the directional coupler
"""

####################################################################################################
####################################################################################################

""" Global Variables """

# Useful variables
ON = 1
OFF = 0

SCOPE_TIME_FACTORS = [1, 2, 5, 10, 20, 50, 100, 200, 500]
SCOPE_VOLTAGE_FACTORS = [1, 2, 5, 10, 20, 50, 100, 200, 500]
SCOPE_TIME_BASE_LIST = []
SCOPE_VOLTAGE_LIST = []
FREQ_LIST = []

####################################################################################################
####################################################################################################
#Helper functions

# def parser():
#     """Perform an arm sweep described by a YAML config file"""
#     parser = argparse.ArgumentParser()
#     parser.add_argument("config_file", help="YAML config file to read from")
#     parser.add_argument(
#         "--override", action="store_true", help="Replace the output database if it already exists"
#     )
#
#     args = parser.parse_args()
#     cfg = measure_waveform_config_from_yaml(args.config_file)
#
#     return [cfg,args.config_file]

#########################################
#########################################

def main():

    # [cfg,config_file] = parser()

    # output file base name
    home = expanduser("~")

    os.chdir(home + "/" + "DataTemp" + "/" + "VRMS_measurements")

    fileName1 = "20180518_03_Signal_Generator_Characterisation_Vrms"
    fileName2 = "20180518_test_Directional_Coupler_Characterisation_Vrms"
    input_file1 = open(fileName1, 'r')
    input_file2 = open(fileName2, 'r')

    inputText1 = input_file1.readlines()
    inputText2 = input_file2.readlines()

    reader1 = csv.reader(inputText1)
    reader2 = csv.reader(inputText2)

    sigGenVrms = []
    directionalCouplerVrms = []
    frequency = []

    next(reader1)
    next(reader2)

    for r in reader1:
        line = r[0].split("\t")

        frequency.append(float(line[0]))
        sigGenVrms.append(float(line[2]))

    for r in reader2:
        line = r[0].split("\t")

        directionalCouplerVrms.append(float(line[2]))


    deltaVrms = []
    vrms99 = []
    vrms99Residual = []
    i=0

    for value in sigGenVrms:
        deltaVrms.append(100*(value - directionalCouplerVrms[i])/value)
        vrms99.append(value*0.99)
        vrms99Residual.append(100*(vrms99[i] - directionalCouplerVrms[i])/vrms99[i])
        i +=1

    figure()

    plt.title('Siggen Vrms vs frequency')
    plt.hold(True)

    reference_bool = False

    legend_list = []

    plt.plot(frequency,sigGenVrms)
    legend_list.append("vrms_siggen")

    plt.plot(frequency,directionalCouplerVrms)
    legend_list.append(("Directional Coupler"))

    plt.plot(frequency, vrms99)
    legend_list.append("99% of siggen")


    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Vrms Voltage (V)')
    plt.legend(legend_list)

    plt.hold(False)

    figure_name = home + "/" + "DataTemp" + "/" + "VRMS_measurements/" + "Siggen_DirCoup_Comparison"
    #print(figure_name)
    plt.savefig(figure_name,format='png')
    plt.show()

    legend_list = []


    plt.plot(frequency, deltaVrms)
    plt.title('delta Vrms')
    legend_list.append("Siggen / dir_coup residual")

    plt.hold(True)
    plt.plot(frequency, vrms99Residual)
    legend_list.append("99% residual")

    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Vrms Voltage (V)')
    plt.legend(legend_list)


    figure_name = home + "/" + "DataTemp" + "/" + "VRMS_measurements/" + "Siggen_DirCoup_%_residual"
    # print(figure_name)
    plt.savefig(figure_name, format='png')
    plt.show()

main()
