
from collections import namedtuple
from schema import Schema, Use, Or, SchemaError
import yaml

SCOPE_SCHEMA = Schema({
    'scope_ip': str,
    'time_base':    Or(int,float),
    'memory_depth':    Or(int),
    'time_base_mode': str,
})
scope = namedtuple('scope', ['scope_ip', 'time_base', 'memory_depth','time_base_mode'])

SCOPE_CHANNEL_SCHEMA = Schema({
    'active': int,
    'channel_scale': Or(int,float),
    'probe_scale': Or(int,float)
})
channel_config = namedtuple('channel_config',['active','channel_scale','probe_scale'])

SIGGEN_SCHEMA = Schema({
    'siggen_ip': str,
    'voltage_mode': str,
    'voltage': Or(int,float),
    'voltage_min': Or(int,float),
    'voltage_max': Or(int,float),
    'voltage_step': Or(int,float),
    'impedance_val': Or(int,float)
})
siggen = namedtuple('siggen_config',['siggen_ip','voltage_mode','voltage','voltage_min','voltage_max','voltage_step','impedance_val'])

FREQUENCY_RANGE_SCHEMA = Schema({
    'low':  Or(int, float),
    'high': Or(int, float),
    'step': Or(int, float),
    'num_points': int
})
FrequencyRange = namedtuple('FrequencyRange', ['low', 'high', 'step','num_points'])

DATA_HEADER_SCHEMA = Schema({
    'author': Or(str),
    'channel_1_connection': Or(str),
    'channel_2_connection': Or(str),
    'channel_3_connection': Or(str),
    'channel_4_connection': Or(str)
})
data_header = namedtuple('data_header', ['author','channel_1_connection','channel_2_connection',
    'channel_3_connection','channel_4_connection'])

CONFIG_SCHEMA = Schema({
    'measurement_name': str,
    'temp_location':    str,
    'data_folder':    str,
    'scope':            SCOPE_SCHEMA.validate,
    'siggen':           SIGGEN_SCHEMA.validate,
    'frequency':        Or(FREQUENCY_RANGE_SCHEMA.validate,int,float),
    'channel_1':   SCOPE_CHANNEL_SCHEMA.validate,
    'channel_2':   SCOPE_CHANNEL_SCHEMA.validate,
    'channel_3':   SCOPE_CHANNEL_SCHEMA.validate,
    'channel_4':   SCOPE_CHANNEL_SCHEMA.validate,
    'data_header': DATA_HEADER_SCHEMA,
    'tags': list
})
Config = namedtuple('config', [
    'measurement_name','temp_location','data_folder','scope', 'siggen',
    'frequency', 'channel_1', 'channel_2', 'channel_3', 'channel_4','data_header', 'tags'
])

def measure_waveform_config_from_yaml(filename):
    cfg = validate_yaml(filename, CONFIG_SCHEMA.validate)
    print("Made it past validation")
    #cfg_freq = cfg['frequency']

    return Config(
        cfg['measurement_name'],
        cfg['temp_location'],
        cfg['data_folder'],
        scope(**cfg['scope']),
        siggen(**cfg['siggen']),
        FrequencyRange(**cfg['frequency']),
        channel_config(**cfg['channel_1']),
        channel_config(**cfg['channel_2']),
        channel_config(**cfg['channel_3']),
        channel_config(**cfg['channel_4']),
        data_header(**cfg['data_header']),
        cfg['tags']

    )


def validate_yaml(filename, validate):
    """Return YAML contents after validating against given function"""
    with open(filename, 'r') as config_file:
        try:
            cfg = yaml.load(config_file)
        except yaml.YAMLError as exc:
            raise ValueError("Config file {} is not valid YAML:\n{}".format(
                filename, exc
            ))

    return validate(cfg)
