import sys, os, time, sqlite3, subprocess, getopt
from rigolFG import RigolFunctionGenerator
from ds1054z import DS1054Z
import matplotlib.pyplot as plt

""" 
This script automates the characterisation of the diretional coupler. 
The script scans the signal generator over different frequencies and VRms values and saves them into a database file
The scope saves the Vrms value for the transmitted and reflected waves.
It stops to allow you to make changes to the setup each time.
Measurements taken are:
Load, open, short, coil
Each component is connected directly to the output port of the directional coupler
"""

####################################################################################################
####################################################################################################

""" DEFAULT Configurations """

# Default output file
sf_name = 'output'
addr_str = '/home/peter/DataTemp/'  # double-check!! temporary directory for the data file while the scan is in progress
DBOutputName = "testDB"
DBName = DBOutputName + ".sqlite"

# Sig gen default frequencies
frequency_low = int(1e5)
frequency_high = int(10e6)
freqStep = int(10000)

freqList = range(frequency_low,frequency_high+1,freqStep)

# Useful variables
ON = 1
OFF = 0

# manual start/end points
manual = False  # if false, -s,-e,-p should be provided

# Default addresses for robotic arm, scope and siggen
#rig_serial_path = '/dev/ttyACM0'
sigGenIP = "192.168.1.156"
scopeIP = '192.168.1.155'

#SigGen parameters
voltageMode = "vrms"
#voltage = 20    #In dBm
voltageMin = 0.1
voltageMax = 3.5
vrmsStepSize = 0.1
impedanceVal = 50    #Impedance setting of the Siggen (Ohms)
vrmsTemp = range(int(voltageMin*10),int((voltageMax+0.1)*10),int(vrmsStepSize*10))
vrmsList = []
for item in vrmsTemp:
    vrmsList.append(item/10)

#Scope parameters
scopeMemoryDepth = 300000
scopeTimeBase = 1e-5
scopeTimeFactors = [1, 2, 5, 10, 20, 50, 100, 200, 500]
scopeTimeBaseList = []
for item in scopeTimeFactors:
    scopeTimeBaseList.append(item*1e-9)
    scopeTimeBaseList.append(item*1e-6)
    scopeTimeBaseList.append(item*1e-3)
####################################################################################################
####################################################################################################
# Command line options

def usage():
    print(sys.argv[0] + ' [OPTIONS]')
    print('OPTIONS:')
    print('\t--freq     (-f) <Frequency to scan (Hz), default = {:s}>'.format(frequency_single))
    print('\t--out      (-o) <output file name, default={:s}>'.format(DBOutputName))
    print('\t--address  (-a) <output file path, default={:s}>'.format(addr_str))
    print('\t--scopeIP  (-x) <Scope IP address, default={:s}>'.format(scopeIP))
    print('\t--sigGenIP (-y) <SigGen IP address, default={:s}>'.format(sigGenIP))
    print(sys.argv[0] + ' -h [--help] (print this help output)')

try:
    cmd_opts, args = getopt.getopt(sys.argv[1:], 'hf:o:a:x:y:',
                                   ['help', 'freq=', 'out=', 'address=','scopeIP=','sigGenIP='])
    print(args)
except getopt.GetoptError as err:
    usage()
    print("The code has stopped here")
    sys.exit(2)

for opt, arg in cmd_opts:

    if opt in ('-f', '--freq'):
        freqTemp = [float(v.strip()) for v in arg.strip().split(',')]
        if(len(freqTemp)==1):
            frequency_single = float(arg.strip())
        elif(len(freqTemp)==3):
            frequency_low = freqTemp[0]
            freqency_high = freqTemp[1]
            freq_step = int(freqTemp[2])

            print(frequency_low)
            print(frequency_high)
            print(freq_step)

            freqList = range(int(frequency_low), int(frequency_high)+1, int(freqStep))
    elif opt in ('-o', '--out'):
        DBName = arg.strip() + '.sqlite'
        #output_name = arg.strip() + '_output.txt'
        print(DBName)

    elif opt in ('-a', '--address'):
        addr_str = arg.strip()

    elif opt in ('-x', '--scopeIP'):
        scopeIP = arg.strip()

    elif opt in ('-y', '--sigGenIP'):
        sigGenIP = arg.strip()

    elif opt in ('-h', '--help'):
        usage()
        sys.exit()


####################################################################################################
####################################################################################################
#Helper functions

def connectToSiggen():
    # Connect to the siggen
    sigGen = RigolFunctionGenerator(host=sigGenIP)
    sigGen.reset()
    return sigGen

def setupSiggen():
    sigGen.setOutput(ON)
    sigGen.setOutputLoad(impedanceVal)
    sigGen.setVoltageUnit(voltageMode)
    sigGen.setVoltage(voltageMin)   #Start from the lowest voltage

def connectToScope():
    # Connect to the scope
    scope = DS1054Z(scopeIP)
    print(scope.idn + "\n")
    return scope

def setupScope():
    # Quickly run the scope so that we can change the number of samples
    scope.run()
    time.sleep(0.2)
    scope.memory_depth = scopeMemoryDepth
    time.sleep(0.2)
    scope.timebase_scale = scopeTimeBase

    # Set the scope time base mode
    scope.write(':Timebase:mode YT')
####################################################################################################
####################################################################################################
#SigGen setup

#Connect to the siggen
sigGen = connectToSiggen()

#Configure the sigGen
setupSiggen()

####################################################################################################
####################################################################################################
#Scope setup

scope = connectToScope()

#Setup scope
setupScope()

####################################################################################################
####################################################################################################
# Setup the database file

if os.path.exists(addr_str + DBName):
        os.remove(addr_str + DBName)

#Create a database to store data
conn = sqlite3.connect(addr_str + DBName)
data = conn.cursor()
data.execute('''CREATE TABLE DirectionalCoupler ('Frequency','VrmsSigGen','VRMS1','VRMS2','Component')''')

####################################################################################################
####################################################################################################
# Run the scan

component = ''

for i in range(1,2,1):

    if(i==1):
        print('Is a load connected? (y/n)')

        while True:
            try:
                inp = input().strip()

                if inp in ['y','Y','yes']:
                    component = 'load'
                    break;

                elif inp in ['n','N', 'no']:
                    print("Please connect a load")

            except:
                print('Please connect a load.')
                continue

    elif (i == 2):
        print('Is a short connected? (y/n)')

        while True:
            try:
                inp = input().strip()

                if inp in ['y', 'Y', 'yes']:
                    component = 'short'
                    break;

                elif inp in ['n', 'N', 'no']:
                    print("Please connect a short")

            except:
                print('Please connect a short.')
                continue

    if (i == 3):
        print('Is a coil connected? (y/n)')

        while True:
            try:
                inp = input().strip()

                if inp in ['y', 'Y', 'yes']:
                    component = 'coil'
                    break;

                elif inp in ['n', 'N', 'no']:
                    print("Please connect a coil")

            except:
                print('Please connect a coil.')
                continue

    print("Starting loop")
    for freq in freqList:

        sigGen.setFrequency(1, freq)
        # Give the sigGen time to setup

        scopeTimeBaseTemp = (1 / freq)
        tempVal = 10000
        for item in scopeTimeBaseList:
            residual = abs(scopeTimeBaseTemp - item)
            if(residual<tempVal):
                tempVal = residual
                scopeTimeBase = item

        print("Timebase is {}".format(scopeTimeBase))

        scope.timebase_scale = scopeTimeBase


        for voltage in vrmsList:

            sigGen.setVoltage(voltage)
            time.sleep(0.1)

            badDataBool = True;

            try:
                while badDataBool:

                    #print("Starting scope")

                    scope.run()

                    time.sleep(0.1)

                    scope.stop()

                    #print("Stopping scope")

                    time.sleep(0.1)

                    channel1VRMS = scope.get_channel_measurement(1,"vrms","current")
                    channel2VRMS = scope.get_channel_measurement(2, "vrms", "current")

                    print("Data read: {}, {}".format(channel1VRMS,channel2VRMS))


                    #badDataBool = False
                    if ((channel1VRMS == None) or (channel2VRMS == None)):
                        badDataBool = True
                        print("Recovering from bad data read")
                    else:
                        badDataBool = False

            except:
                # If the data wasn't read from the scope properly, reconnect to scope and sigen and try again to read.
                errorBool = True

                while errorBool:
                    print("Recovering from thrown exception")
                    try:
                        scope = connectToScope()
                        setupScope()
                        sigGen = connectToSiggen()
                        setupSiggen()
                        sigGen.setFrequency(1, freq)
                        time.sleep(1)

                        scope.single()

                        time.sleep(0.2)

                        channel1VRMS = scope.get_channel_measurement(1, "vrms", "current")
                        channel2VRMS = scope.get_channel_measurement(2, "vrms", "current")

                        errorBool = False

                        if((channel1VRMS==None) or (channel2VRMS==None)):
                            errorBool = True
                        else:
                            errorBool = False
                    except:
                        errorBool = True


            data.execute("INSERT INTO DirectionalCoupler VALUES (?,?,?,?,?)", [float(freq),float(voltage),float(channel1VRMS), float(channel2VRMS),str(component)])
            conn.commit()
            sys.stdout.flush()


####################################################################################################
####################################################################################################
# Park the arm and disconnect to all

conn.close()


