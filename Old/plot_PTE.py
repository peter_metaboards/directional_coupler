# /usr/bin/env python3
# TRIPLE-check the name_tree!!! specific for each tile type, load, control type type and array arquitecture
# to label the output files properly, the code requires:
#			 the probe height in LEGO bricks (e.g. -z 5)
#			 the number of tiles that are switched off (e.g. -o 23,32), if non given - the code assumes no defects
#            the db filename (e.g. -d filename)
#
import sqlite3
import time
import datetime
import os, sys, getopt
import math
import subprocess

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cm
from pylab import *

import lib.sqlite_manipulation as sql2sp

from operator import itemgetter, attrgetter


def usage():
    print(sys.argv[0] + ' [OPTIONS]')
    print('OPTIONS:')
    print(sys.argv[0] + ' -h [--help] (print this help output)')


try:
    cmd_opts, args = getopt.getopt(sys.argv[1:], 'h', ['help'])
except getopt.GetoptError as err:
    usage()
    sys.exit(2)

try:
    db_name = str(sys.argv[1])
    db_name2 = str(sys.argv[2])
except:
    print('Please specify the database name as an argument')
    sys.exit()


# create directory for all PNGs
#
# print('Please specify the plot(s) subtitle')
# try:
#     suptitle_text = str(input().strip())
# except:
#     print('text not recognised')
#     sys.exit()

table = 'DirectionalCoupler'
print(db_name)

VrmsSigGen = sql2sp.read_sql_coords_table(db_name,table,"VrmsSigGen")
Frequency = sql2sp.read_sql_coords_table(db_name,table,"Frequency")

VRMS1List = []
VRMS2List = []
VSigGenList = []
VSigGenListOnly = []
VRMS1ResidualList = []
VSigGenMeasList = []
VSigGenMeasListOnly = []
scaleFactor = 100.1

for value in range(1,5,1):

    VrmsSigGenVoltage = value*0.5
    #print(Frequency)
    #print(VrmsSigGen)

    conn = sqlite3.connect(db_name)
    conn2 = sqlite3.connect(db_name2)
    c = conn.cursor()
    c2 = conn2.cursor()

    c.execute('''SELECT VRMS1, VRMS2 FROM DirectionalCoupler WHERE VrmsSigGen = {}'''.format(VrmsSigGenVoltage))
    c2.execute('''SELECT VRMS1 FROM DirectionalCoupler WHERE VrmsSigGen = {}'''.format(VrmsSigGenVoltage))

    temp = c.fetchall()
    VRMS1 = []
    VRMS2 = []
    VSigGen = []
    VRMS1Residual = []

    for item in temp:
        VRMS1.append(item[0])
        VRMS2.append(item[1])

    temp2 = c2.fetchall()
    VSigGenMeas = []
    VSigGen100 = []
    VSigGenMeas100 = []

    for item in temp2:
        VSigGenMeas.append(item[0])
        VSigGenMeas100.append(item[0]/scaleFactor)

    print(len(VRMS1))

    for i in range (0,len(VRMS1),1):
        print(i)
        tempVal = VRMS1[i] - VSigGenMeas100[i]
        VRMS1Residual.append(tempVal)

    #Create the straight lines in the plots
    for item in Frequency:
        VSigGen100.append(VrmsSigGenVoltage/100)
        VSigGen.append(VrmsSigGenVoltage)

    VRMS1List.append(VRMS1)
    VRMS2List.append(VRMS2)
    VSigGenListOnly.append(VSigGen)
    VSigGenList.append(VSigGen100)
    VSigGenMeasList.append(VSigGenMeas100)
    VSigGenMeasListOnly.append(VSigGenMeas)
    VRMS1ResidualList.append(VRMS1Residual)


figure()
plt.title('Vrms Siggen voltages')
plt.hold
i=0

plt.plot(Frequency,VSigGenListOnly[3],Frequency,VSigGenMeasListOnly[3])

#[p,V] = np.polyfit(Frequency,VRMS1List[3],5)

plt.xlabel('Frequency (Hz)')
plt.ylabel('RMS Voltage (V)')

############


figure()
plt.title('Vrms Siggen voltages')
plt.hold
i=0

plt.plot(Frequency,VRMS1List[3],Frequency,VSigGenList[3],Frequency,VSigGenMeasList[3])

#[p,V] = np.polyfit(Frequency,VRMS1List[3],5)

plt.xlabel('Frequency (Hz)')
plt.ylabel('RMS Voltage (V)')

############

figure()
plt.title('Vrms Siggen voltages')
plt.hold
i=0

for item in VRMS1List:
    plt.plot(Frequency,item,Frequency,VSigGenList[i],Frequency,VSigGenMeasList[i])
    i+=1

plt.xlabel('Frequency (Hz)')
plt.ylabel('RMS Voltage (V)')

###########
figure()
plt.title('Residual')

i=0
for item in VRMS1List:
    plt.plot(Frequency,VRMS1ResidualList[i])
    i+=1

plt.xlabel('Frequency (Hz)')
plt.ylabel('RMS Voltage (V)')

plt.show()



# for zi in range(len(zcoords)):
#     s11, s12, s21, s22 = sql2sp.read_sql_sp(db_name, fs, z=zcoords[zi])
#     m11[:, zi], p11[:, zi] = sql2sp.sql2dB(s11)
#     m21[:, zi], p21[:, zi] = sql2sp.sql2dB(s21)
#
# va = round(np.max(m11), 1)
# vi = round(np.min(m11), 1)
# vab = round(np.max(m21), 1)
# vib = round(np.min(m21), 1)
#
# for f in range(len(fs)):
#     m11_at_f[:, :] = m11[f, :, :]
#     p11_at_f[:, :] = p11[f, :, :]
#     m21_at_f[:, :] = m21[f, :, :]
#     p21_at_f[:, :] = p21[f, :, :]
#
#     figure()
#     plt.suptitle(suptitle_text + ' (f=' + str(int(fs[f] / 1000)) + 'kHz)')
#     plt.subplot(221)
#     plt.contourf(xcoords, ycoords, m11_at_f, cmap=plt.cm.hot, levels=np.linspace(vi, va, 9), vmin=vi, vmax=va)
#     plt.title('mag(S11), dB')
#     plt.xlabel('x (mm)')
#     plt.ylabel('y (mm)')
#     plt.colorbar()
#
#     plt.subplot(222)
#     plt.contourf(xcoords, ycoords, p11_at_f, cmap=plt.cm.seismic, levels=np.linspace(-180, 180, 19), vmin=-180,
#                  vmax=180)
#     plt.title('phase(S11), degrees')
#     plt.xlabel('x (mm)')
#     plt.ylabel('y (mm)')
#     plt.colorbar()
#
#     plt.subplot(223)
#     plt.contourf(xcoords, ycoords, m21_at_f, cmap=plt.cm.hot, levels=np.linspace(vib, vab, 9), vmin=vib, vmax=vab)
#     plt.title('mag(S21), dB')
#     plt.xlabel('x (mm)')
#     plt.ylabel('y (mm)')
#     plt.colorbar()
#
#     plt.subplot(224)
#     plt.contourf(xcoords, ycoords, p21_at_f, cmap=plt.cm.seismic, levels=np.linspace(-180, 180, 19), vmin=-180,
#                  vmax=180)
#     plt.title('phase(S21), degrees')
#     plt.xlabel('x (mm)')
#     plt.ylabel('y (mm)')
#     plt.colorbar()
#
#     plt.subplots_adjust(hspace=.5, wspace=.5)
#     plt.savefig(dirname + '/S-parameters_' + name_tree + '_f_' + str(int(fs[f])) + '.png')
#
#     plt.close()
#
# os.system(
#     'convert -delay 5 ' + dirname + '/*.png -quality 100% -compress None -loop 0 ' + name_tree + 'dB_animation.mp4')
#
