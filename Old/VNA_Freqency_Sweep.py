import sys, os, time, sqlite3, subprocess, getopt
from pylab import *
from lib.vna import VNA


#Default output file
sf_name = 'output'
addr_str = '/home/peter/DataTemp/'  # double-check!! temporary directory for the data file while the scan is in progress
DBOutputName = "VNAData"
DBName = DBOutputName + ".sqlite"

v = VNA("192.168.1.199")


if os.path.exists(addr_str + DBName):
        os.remove(addr_str + DBName)

#Create a database to store data
conn = sqlite3.connect(addr_str + DBName)
data = conn.cursor()
data.execute('''CREATE TABLE Sparameters
             ('Z','frequency (Hz)', 'mag(S11)', 'rad(S11)','mag(S12)','rad(S12)','mag(S21)', 'rad(S21)','mag(S22)','rad(S22)')''')

# output_list=['#tile','resonance frequency(MHz)','quality factor']
for z in range(1,2,1):

    print('Has a layer been added? (y/n)')

    while True:
        try:
            inp = input().strip()

            if inp in ['y', 'Y', 'yes']:
                break;

            elif inp in ['n', 'N', 'no']:
                print("Please add the next layer")

        except:
            print('Please add a layer.')
            continue

    currentZ = z*32

    print(currentZ)

    S_raw = None

    while True:
        S_raw = v.meas_two_port(timeout=120)

        if(S_raw == None):
            print("Measurement error!")
        else:
            break;

    s11 = S_raw.s11
    s21 = S_raw.s21
    s12 = S_raw.s12
    s22 = S_raw.s22

    freq = s21.f

    for i_meas in range(len(s11.f)):
        data.execute("INSERT INTO Sparameters VALUES (?,?,?,?,?,?,?,?,?,?)", [float(currentZ), s11.f[i_meas], s11.s_mag[i_meas, 0, 0],
                      s11.s_rad[i_meas, 0, 0], s12.s_mag[i_meas, 0, 0], s12.s_rad[i_meas, 0, 0],
                      s21.s_mag[i_meas, 0, 0], s21.s_rad[i_meas, 0, 0], s22.s_mag[i_meas, 0, 0],
                      s22.s_rad[i_meas, 0, 0]])
    conn.commit()
    sys.stdout.flush()


print()

