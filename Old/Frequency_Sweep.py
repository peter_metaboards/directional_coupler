import sys, os, sqlite3, subprocess, getopt, math
import time as time
from rigolFG import RigolFunctionGenerator
from ds1054z import DS1054Z
import matplotlib.pyplot as plt

""" 
This script automates the characterisation of the diretional coupler. 
The script scans the signal generator over different frequencies and VRms values and saves them into a database file
The scope saves the Vrms value for the transmitted and reflected waves.
It stops to allow you to make changes to the setup each time.
Measurements taken are:
Load, open, short, coil
Each component is connected directly to the output port of the directional coupler
"""

####################################################################################################
####################################################################################################

""" DEFAULT Configurations """

# Default output file
sf_name = 'output'
addr_str = '/home/peter/DataTemp/'  # double-check!! temporary directory for the data file while the scan is in progress
DBOutputName = "testDB"
DBName = DBOutputName + ".sqlite"

# Sig gen default frequencies
# frequency_low = int(100e3)
# frequency_high = int(300e3)
frequency_low = int(6.4e6)
frequency_high = int(7.2e6)
numPoints = 800
freqStep = (frequency_high - frequency_low)/numPoints

freqList = []
freqList.append(frequency_low)
temp = frequency_low

for i in range(1,numPoints+1,1):
    freqList.append(temp + freqStep)
    temp = temp + freqStep


# Useful variables
ON = 1
OFF = 0

# manual start/end points
manual = False  # if false, -s,-e,-p should be provided

# Default addresses for robotic arm, scope and siggen
#rig_serial_path = '/dev/ttyACM0'
sigGenIP = "192.168.1.156"
scopeIP = '192.168.1.155'

#SigGen parameters
voltageMode = "dbm"
#voltage = 20    #In dBm
voltageMin = 0.1
voltageMax = 3.5
vrmsStepSize = 0.1
impedanceVal = 50    #Impedance setting of the Siggen (Ohms)
vrmsTemp = range(int(voltageMin*10),int((voltageMax+0.1)*10),int(vrmsStepSize*10))
vrmsList = []
for item in vrmsTemp:
    vrmsList.append(item/10)

#Scope parameters
scopeMemoryDepth = 300000
scopeTimeBase = 1e-5
scopeTimeFactors = [1, 2, 5, 10, 20, 50, 100, 200, 500]
scopeTimeBaseList = []
for item in scopeTimeFactors:
    scopeTimeBaseList.append(item*1e-9)
    scopeTimeBaseList.append(item*1e-6)
    scopeTimeBaseList.append(item*1e-3)

scopeVoltageFactors = [1, 2, 5, 10, 20, 50, 100, 200, 500]
scopeVoltageList = []
for item in scopeVoltageFactors:
    scopeVoltageList.append(item)
    scopeVoltageList.append(item*1e-3)
####################################################################################################
####################################################################################################
# Command line options

def usage():
    print(sys.argv[0] + ' [OPTIONS]')
    print('OPTIONS:')
    print('\t--freq     (-f) <Frequency to scan (Hz), default = {:s}>'.format(frequency_single))
    print('\t--out      (-o) <output file name, default={:s}>'.format(DBOutputName))
    print('\t--address  (-a) <output file path, default={:s}>'.format(addr_str))
    print('\t--scopeIP  (-x) <Scope IP address, default={:s}>'.format(scopeIP))
    print('\t--sigGenIP (-y) <SigGen IP address, default={:s}>'.format(sigGenIP))
    print(sys.argv[0] + ' -h [--help] (print this help output)')

try:
    cmd_opts, args = getopt.getopt(sys.argv[1:], 'hf:o:a:x:y:',
                                   ['help', 'freq=', 'out=', 'address=','scopeIP=','sigGenIP='])
    print(args)
except getopt.GetoptError as err:
    usage()
    print("The code has stopped here")
    sys.exit(2)

for opt, arg in cmd_opts:

    if opt in ('-f', '--freq'):
        freqTemp = [float(v.strip()) for v in arg.strip().split(',')]
        if(len(freqTemp)==1):
            frequency_single = float(arg.strip())
        elif(len(freqTemp)==3):
            frequency_low = freqTemp[0]
            frequency_high = freqTemp[1]
            freq_step = int(freqTemp[2])

            print(frequency_low)
            print(frequency_high)
            print(freq_step)

            freqList = range(int(frequency_low), int(frequency_high)+1, int(freqStep))
            print(freqList)
    elif opt in ('-o', '--out'):
        DBName = arg.strip() + '.sqlite'
        #output_name = arg.strip() + '_output.txt'
        print(DBName)

    elif opt in ('-a', '--address'):
        addr_str = arg.strip()

    elif opt in ('-x', '--scopeIP'):
        scopeIP = arg.strip()

    elif opt in ('-y', '--sigGenIP'):
        sigGenIP = arg.strip()

    elif opt in ('-h', '--help'):
        usage()
        sys.exit()


####################################################################################################
####################################################################################################
#Helper functions

def connectToSiggen():
    # Connect to the siggen
    sigGen = RigolFunctionGenerator(host=sigGenIP)
    sigGen.reset()
    return sigGen

def setupSiggen():
    sigGen.setOutput(ON)
    sigGen.setOutputLoad(impedanceVal)
    sigGen.setVoltageUnit(voltageMode)
    sigGen.setVoltage(voltageMin)   #Start from the lowest voltage

def connectToScope():
    # Connect to the scope
    scope = DS1054Z(scopeIP)
    print(scope.idn + "\n")
    return scope

def setupScope():
    # Quickly run the scope so that we can change the number of samples
    scope.run()
    time.sleep(0.2)
    scope.memory_depth = scopeMemoryDepth
    time.sleep(0.2)
    scope.timebase_scale = scopeTimeBase

    # Set the scope time base mode
    scope.write(':Timebase:mode YT')

def checkVerticalRange(channelNo):

    rangeChanged = True
    while rangeChanged:
        VRange = (scope.get_channel_scale(channelNo))
        VRMS = scope.get_channel_measurement(channelNo, "vrms", "current")
        if(VRMS == None):

            while True:
                VRange = (scope.get_channel_scale(channelNo))
                VRMS = scope.get_channel_measurement(channelNo, "vrms", "current")
                print(VRMS)
                if(VRMS == None):
                    tempVal = 10000
                    for item in scopeVoltageList:
                        residual = abs((VRange * 2) - item)
                        if (residual < tempVal):
                            tempVal = residual
                            NewVRange = item
                    print("{},{}".format(VRange,NewVRange))

                    scope.set_channel_scale(channelNo, NewVRange, True)
                    time.sleep(0.5)
                    print("Decreasing range to get data")
                else:
                    break
                #time.sleep(0.2)
            time.sleep(0.3)
        if(VRMS > (2.5*VRange)):
            rangeChanged = True
            tempVal = 10000
            for item in scopeVoltageList:

                residual = abs((VRange*2) - item)
                if(residual<tempVal):
                    tempVal = residual
                    NewVRange = item
            print("Reducing range to prevent clipping")

            scope.set_channel_scale(channelNo,NewVRange,True)
            time.sleep(0.3)

        elif((VRMS < (VRange)) and (VRange != 1e-3)):
            scaleFactor = VRange / VRMS
            scaleFactor2 = math.floor(scaleFactor/2)
            print(scaleFactor)
            rangeChanged = True
            tempVal = 10000
            for item in scopeVoltageList:

                residual = abs((VRange / (2**scaleFactor2)) - item)
                if (residual < tempVal):
                    tempVal = residual
                    NewVRange = item
            print("Increasing range to ensure good resolution")
            scope.set_channel_scale(channelNo, NewVRange, True)
            time.sleep(0.3)
        else:
            #print("No changes made")
            rangeChanged = False



####################################################################################################
####################################################################################################
#SigGen setup

#Connect to the siggen
sigGen = connectToSiggen()

#Configure the sigGen
setupSiggen()

####################################################################################################
####################################################################################################
#Scope setup

scope = connectToScope()

#Setup scope
setupScope()

####################################################################################################
####################################################################################################
# Setup the database file

if os.path.exists(addr_str + DBName):
        os.remove(addr_str + DBName)

#Create a database to store data
conn = sqlite3.connect(addr_str + DBName)
data = conn.cursor()
data.execute('''CREATE TABLE DirectionalCoupler ('Z','Frequency','VrmsLoad','VRMS1','VRMS2','Component')''')

####################################################################################################
####################################################################################################
# Run the scan

component = ''
sigGen.setVoltage(0)

for z in range(1,2,1):

    print('Has a layer been added? (y/n)')

    while True:
        try:
            inp = input().strip()

            if inp in ['y', 'Y', 'yes']:
                break;

            elif inp in ['n', 'N', 'no']:
                print("Please add the next layer")

        except:
            print('Please add a layer.')
            continue

    currentZ = z*32

    print(currentZ)

    for freq in freqList:

        sigGen.setFrequency(1, freq)
        # Give the sigGen time to setup

        scopeTimeBaseTemp = (1 / freq)
        tempVal = 10000
        for item in scopeTimeBaseList:
            residual = abs(scopeTimeBaseTemp - item)
            if(residual<tempVal):
                tempVal = residual
                scopeTimeBase = item

        #print("Timebase is {}".format(scopeTimeBase))

        scope.timebase_scale = scopeTimeBase

        #time.sleep(0.1)

        badDataBool = True;

        try:
            while badDataBool:

                #print("Starting scope")

                scope.run()

                checkVerticalRange(1)
                checkVerticalRange(2)
                checkVerticalRange(3)
                #time.sleep(0.1)

                scope.stop()

                #print("Stopping scope")

                time.sleep(0.05)

                channel1VRMS = scope.get_channel_measurement(1,"vrms","current")
                channel2VRMS = scope.get_channel_measurement(2, "vrms", "current")
                channel3VRMS = scope.get_channel_measurement(3, "vrms", "current")

                #print("Data read: {}, {}, {}".format(channel1VRMS,channel2VRMS,channel3VRMS))


                #badDataBool = False
                if ((channel1VRMS == None) or (channel2VRMS == None) or (channel3VRMS == None)):
                    badDataBool = True
                    print("Recovering from bad data read")
                else:
                    badDataBool = False

        except:
            # If the data wasn't read from the scope properly, reconnect to scope and sigen and try again to read.
            errorBool = True

            while errorBool:
                print("Recovering from thrown exception")
                try:
                    scope = connectToScope()
                    setupScope()
                    sigGen = connectToSiggen()
                    setupSiggen()
                    sigGen.setFrequency(1, freq)
                    time.sleep(1)

                    scope.run()

                    # time.sleep

                    scope.stop()

                    time.sleep(0.05)

                    channel1VRMS = scope.get_channel_measurement(1, "vrms", "current")
                    channel2VRMS = scope.get_channel_measurement(2, "vrms", "current")
                    channel3VRMS = scope.get_channel_measurement(3, "vrms", "current")

                    errorBool = False

                    if((channel1VRMS==None) or (channel2VRMS==None) or (channel3VRMS==None)):
                        errorBool = True
                    else:
                        errorBool = False
                except:
                    errorBool = True

        print("{},{},{},{},{},{}".format(float(currentZ),float(freq),float(channel1VRMS), float(channel2VRMS),float(channel3VRMS),str(component)))

        data.execute("INSERT INTO DirectionalCoupler VALUES (?,?,?,?,?,?)", [float(currentZ),float(freq),float(channel1VRMS), float(channel2VRMS),float(channel3VRMS),str(component)])
        conn.commit()
        sys.stdout.flush()


####################################################################################################
####################################################################################################
# Park the arm and disconnect to all

conn.close()


